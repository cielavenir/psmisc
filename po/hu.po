# Hungarian translation of psmisc
# Copyright (C) 2006, 2007, 2009, 2012, 2014, 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the psmisc package.
#
# Emese Kovacs <emese@instantweb.hu>, 2006.
# Gabor Kelemen <kelemeng@gnome.hu>, 2006, 2007, 2009, 2012.
# Balázs Úr <urbalazs@gmail.com>, 2014, 2017.
msgid ""
msgstr ""
"Project-Id-Version: psmisc 23.0-rc1\n"
"Report-Msgid-Bugs-To: csmall@dropbear.xyz\n"
"POT-Creation-Date: 2021-02-06 11:02+1100\n"
"PO-Revision-Date: 2017-04-27 22:05+0200\n"
"Last-Translator: Balázs Úr <urbalazs@gmail.com>\n"
"Language-Team: Hungarian <translation-team-hu@lists.sourceforge.net>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: src/fuser.c:145
#, c-format
msgid ""
"Usage: fuser [-fIMuvw] [-a|-s] [-4|-6] [-c|-m|-n SPACE]\n"
"             [-k [-i] [-SIGNAL]] NAME...\n"
"       fuser -l\n"
"       fuser -V\n"
"Show which processes use the named files, sockets, or filesystems.\n"
"\n"
"  -a,--all              display unused files too\n"
"  -i,--interactive      ask before killing (ignored without -k)\n"
"  -I,--inode            use always inodes to compare files\n"
"  -k,--kill             kill processes accessing the named file\n"
"  -l,--list-signals     list available signal names\n"
"  -m,--mount            show all processes using the named filesystems or\n"
"                        block device\n"
"  -M,--ismountpoint     fulfill request only if NAME is a mount point\n"
"  -n,--namespace SPACE  search in this name space (file, udp, or tcp)\n"
"  -s,--silent           silent operation\n"
"  -SIGNAL               send this signal instead of SIGKILL\n"
"  -u,--user             display user IDs\n"
"  -v,--verbose          verbose output\n"
"  -w,--writeonly        kill only processes with write access\n"
"  -V,--version          display version information\n"
msgstr ""
"Használat: fuser [-fIMuvw] [-a|-s] [-4|-6] [-c|-m|-n NÉVTÉR]\n"
"                 [-k [-i] [-SZIGNÁL]] NÉV...\n"
"       fuser -l\n"
"       fuser -V\n"
"Megjeleníti a megnevezett fájlokat, foglalatokat vagy fájlrendszereket "
"használó\n"
"folyamatokat.\n"
"\n"
"  -a,--all              a nem használt fájlok is jelenjenek meg\n"
"  -i,--interactive      kérdés kilövés előtt (-k nélkül figyelmen kívül "
"marad)\n"
"  -I,--inode            mindig használjon inode-okat fájlok "
"összehasonlításához\n"
"  -k,--kill             a megnevezett fájlokhoz hozzáférő folyamatok "
"kilövése\n"
"  -l,--list-signals     elérhető szignálnevek felsorolása\n"
"  -m,--mount            a megnevezett fájlrendszereket vagy blokkeszközöket\n"
"                          használó összes folyamat megjelenítése\n"
"  -M,--ismountpoint     kérés teljesítése csak ha a NÉV egy csatolási pont\n"
"  -n,--namespace NÉVTÉR keresés ebben a névtérben (file, udp vagy tcp)\n"
"  -s,--silent           néma működés\n"
"  -SZIGNÁL              ezen szignál elküldése a SIGKILL helyett\n"
"  -u,--user             felhasználói azonosítók megjelenítése\n"
"  -v,--verbose          részletes kimenet\n"
"  -w,--writeonly        csak írási hozzáféréssel rendelkező folyamatok "
"kilövése\n"
"  -V,--version          verzióinformációk megjelenítése\n"

#: src/fuser.c:166
#, c-format
msgid ""
"  -4,--ipv4             search IPv4 sockets only\n"
"  -6,--ipv6             search IPv6 sockets only\n"
msgstr ""
"  -4,--ipv4             csak IPv4 foglalatok keresése\n"
"  -6,--ipv6             csak IPv6 foglalatok keresése\n"

#: src/fuser.c:169
#, c-format
msgid ""
"  -                     reset options\n"
"\n"
"  udp/tcp names: [local_port][,[rmt_host][,[rmt_port]]]\n"
"\n"
msgstr ""
"  -                     kapcsolók visszaállítása\n"
"\n"
"  udp/tcp nevek: [helyi_port][,[távoli_gép][,[távoli_port]]]\n"
"\n"

#: src/fuser.c:176
#, c-format
msgid "fuser (PSmisc) %s\n"
msgstr "fuser (PSmisc) %s\n"

#: src/fuser.c:179 src/pstree.c:1266
#, fuzzy, c-format
msgid ""
"Copyright (C) 1993-2020 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2010 Werner Almesberger és Craig Small\n"
"\n"

#: src/fuser.c:181 src/killall.c:839 src/peekfd.c:195 src/prtstat.c:68
#: src/pstree.c:1268
#, c-format
msgid ""
"PSmisc comes with ABSOLUTELY NO WARRANTY.\n"
"This is free software, and you are welcome to redistribute it under\n"
"the terms of the GNU General Public License.\n"
"For more information about these matters, see the files named COPYING.\n"
msgstr ""
"A PSmisc csomagra nem vonatkozik ABSZOLÚT SEMMILYEN GARANCIA\n"
"Ez egy szabad szoftver, a GNU General Public License feltételei mellett\n"
"bármikor továbbíthatja, a részletekért lásd a COPYING fájlt.\n"

#: src/fuser.c:203
#, c-format
msgid "Cannot open /proc directory: %s\n"
msgstr "A /proc könyvtár nem nyitható meg: %s\n"

#: src/fuser.c:394 src/fuser.c:447 src/fuser.c:2078
#, c-format
msgid "Cannot allocate memory for matched proc: %s\n"
msgstr "Nem foglalható memória az illeszkedő folyamat részére: %s\n"

#: src/fuser.c:474
#, c-format
msgid "Specified filename %s does not exist.\n"
msgstr "A megadott %s fájlnév nem létezik.\n"

#: src/fuser.c:477
#, c-format
msgid "Cannot stat %s: %s\n"
msgstr "%s nem érhető el: %s\n"

#: src/fuser.c:614
#, c-format
msgid "Cannot resolve local port %s: %s\n"
msgstr "A(z) %s helyi port nem oldható fel: %s\n"

#: src/fuser.c:634
#, c-format
msgid "Unknown local port AF %d\n"
msgstr "Ismeretlen helyi port AF: %d\n"

#: src/fuser.c:723
#, c-format
msgid "Cannot open protocol file \"%s\": %s\n"
msgstr "Nem nyitható meg a(z) \"%s\" protokollfájl: %s\n"

#: src/fuser.c:1026
#, c-format
msgid "Specified filename %s is not a mountpoint.\n"
msgstr "A megadott %s fájlnév nem csatolási pont.\n"

#: src/fuser.c:1118
#, c-format
msgid "%s: Invalid option %s\n"
msgstr "%s: érvénytelen kapcsoló: %s\n"

#: src/fuser.c:1172
msgid "Namespace option requires an argument."
msgstr "A névtér kapcsoló argumentumot igényel."

#: src/fuser.c:1189
msgid "Invalid namespace name"
msgstr "Érvénytelen névtérnév"

#: src/fuser.c:1254
msgid "You can only use files with mountpoint options"
msgstr "Fájlok csak csatolási pont kapcsolókkal használhatók"

#: src/fuser.c:1304
msgid "No process specification given"
msgstr "Nincs megadva folyamatmeghatározás"

#: src/fuser.c:1319
msgid "all option cannot be used with silent option."
msgstr "az összes kapcsoló nem használható a néma kapcsolóval."

#: src/fuser.c:1324
msgid "You cannot search for only IPv4 and only IPv6 sockets at the same time"
msgstr "Nem lehet egyszerre csak IPv4 és csak IPv6 foglalatokat keresni"

#: src/fuser.c:1413
#, c-format
msgid "%*s USER        PID ACCESS COMMAND\n"
msgstr "%*s FELHASZNÁLÓ  PID HOZZÁFÉRÉS PARANCS\n"

#: src/fuser.c:1446 src/fuser.c:1503
msgid "(unknown)"
msgstr "(ismeretlen)"

#: src/fuser.c:1588 src/fuser.c:1638
#, c-format
msgid "Cannot stat file %s: %s\n"
msgstr "A(z) %s fájl nem érhető el: %s\n"

#: src/fuser.c:1731
#, c-format
msgid "Cannot open /proc/net/unix: %s\n"
msgstr "A /proc/net/unix könyvtár nem nyitható meg: %s\n"

#: src/fuser.c:1826
#, c-format
msgid "Kill process %d ? (y/N) "
msgstr "Kilövi a(z) %d folyamatot? (i/N) "

#: src/fuser.c:1862
#, c-format
msgid "Could not kill process %d: %s\n"
msgstr "A(z) %d folymat kilövése nem sikerült: %s\n"

#: src/fuser.c:1877
#, c-format
msgid "Cannot open a network socket.\n"
msgstr "Nem nyitható meg hálózati foglalat\n"

#: src/fuser.c:1881
#, c-format
msgid "Cannot find socket's device number.\n"
msgstr "Nem található a foglalat eszközszáma.\n"

#: src/killall.c:109
#, c-format
msgid "Kill %s(%s%d) ? (y/N) "
msgstr "Kilövi ezt: %s(%s%d) ? (i/N) "

#: src/killall.c:112
#, c-format
msgid "Signal %s(%s%d) ? (y/N) "
msgstr "Elküldi a(z) %s(%s%d) szignált? (i/N) "

#: src/killall.c:255
#, c-format
msgid "killall: Cannot get UID from process status\n"
msgstr ""
"killall: Nem kérhető le a felhasználói azonosító a folyamat állapotából\n"

#: src/killall.c:344
#, c-format
msgid "killall: Bad regular expression: %s\n"
msgstr "killall: Hibás reguláris kifejezés: %s\n"

#: src/killall.c:501
#, c-format
msgid "killall: skipping partial match %s(%d)\n"
msgstr "killall: %s(%d) részleges találat átlépése\n"

#: src/killall.c:744
#, c-format
msgid "Killed %s(%s%d) with signal %d\n"
msgstr "%s(%s%d) kilőve a(z) %d szignállal\n"

#: src/killall.c:763
#, c-format
msgid "%s: no process found\n"
msgstr "%s: nem található folyamat\n"

#: src/killall.c:804
#, c-format
msgid "Usage: killall [OPTION]... [--] NAME...\n"
msgstr "Használat: killall [KAPCSOLÓ]... [--] NÉV...\n"

#: src/killall.c:806
#, fuzzy, c-format
msgid ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          require exact match for very long names\n"
"  -I,--ignore-case    case insensitive process name match\n"
"  -g,--process-group  kill process group instead of process\n"
"  -y,--younger-than   kill processes younger than TIME\n"
"  -o,--older-than     kill processes older than TIME\n"
"  -i,--interactive    ask for confirmation before killing\n"
"  -l,--list           list all known signal names\n"
"  -q,--quiet          don't print complaints\n"
"  -r,--regexp         interpret NAME as an extended regular expression\n"
"  -s,--signal SIGNAL  send this signal instead of SIGTERM\n"
"  -u,--user USER      kill only process(es) running as USER\n"
"  -v,--verbose        report if the signal was successfully sent\n"
"  -V,--version        display version information\n"
"  -w,--wait           wait for processes to die\n"
"  -n,--ns PID         match processes that belong to the same namespaces\n"
"                      as PID\n"
msgstr ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          megköveteli a pontos egyezést nagyon hosszú nevek "
"esetén;\n"
"  -I,--ignore-case    a folyamatnév-illesztés ne legyen kis- és\n"
"                        nagybetűérzékeny\n"
"  -g,--process-group  folyamatcsoport kilövése folyamat helyett\n"
"  -y,--younger-than   az IDŐNÉL fiatalabb folyamatok kilövése\n"
"  -o,--older-than     az IDŐNÉL öregebb folyamatok kilövése\n"
"  -i,--interactive    megerősítés kérése kilövés előtt\n"
"  -l,--list           az összes ismert szignálnév felsorolása\n"
"  -q,--quiet          ne kérdezzen\n"
"  -r,--regexp         a NÉV értelmezése kiterjesztett reguláris "
"kifejezésként\n"
"  -s,--signal SZIGNÁL ezen szignál elküldése a SIGTERM helyett\n"
"  -u,--user FELHASZNÁLÓ  csak a FELHASZNÁLÓ nevében futó folyamatok "
"kilövése\n"
"  -v,--verbose        értesítés, ha a szignál küldése sikeres\n"
"  -V,--version        verzióinformációk megjelenítése\n"
"  -w,--wait           várja meg, amíg a folyamatok meghalnak\n"

#: src/killall.c:826
#, c-format
msgid ""
"  -Z,--context REGEXP kill only process(es) having context\n"
"                      (must precede other arguments)\n"
msgstr ""
"  -Z,--context REGEXP csak a kontextussal rendelkező folyamatok kilövése\n"
"                      (meg kell előznie az egyéb argumentumokat)\n"

#: src/killall.c:837
#, fuzzy, c-format
msgid ""
"Copyright (C) 1993-2021 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2010 Werner Almesberger és Craig Small\n"
"\n"

#: src/killall.c:922 src/killall.c:928
msgid "Invalid time format"
msgstr "Érvénytelen időformátum"

#: src/killall.c:948
#, c-format
msgid "Cannot find user %s\n"
msgstr "A(z) %s felhasználó nem található\n"

#: src/killall.c:983
#, fuzzy
msgid "Invalid namespace PID"
msgstr "Érvénytelen névtérnév"

#: src/killall.c:990
#, c-format
msgid "Bad regular expression: %s\n"
msgstr "Hibás reguláris kifejezés: %s\n"

#: src/killall.c:1024
#, c-format
msgid "killall: Maximum number of names is %d\n"
msgstr "killall: A nevek maximális száma %d\n"

#: src/killall.c:1029
#, c-format
msgid "killall: %s lacks process entries (not mounted ?)\n"
msgstr "killall: %s nem tartalmaz folyamatbejegyzéseket (nincs csatolva?)\n"

#: src/peekfd.c:183
#, c-format
msgid "Error attaching to pid %i\n"
msgstr "Hiba a csatlakozás közben a(z) %i folyamatazonosítóhoz\n"

#: src/peekfd.c:191
#, c-format
msgid "peekfd (PSmisc) %s\n"
msgstr "peekfd (PSmisc) %s\n"

#: src/peekfd.c:193
#, c-format
msgid ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"
msgstr ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"

#: src/peekfd.c:203
#, fuzzy, c-format
msgid ""
"Usage: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8, --eight-bit-clean        output 8 bit clean streams.\n"
"    -n, --no-headers             don't display read/write from fd headers.\n"
"    -c, --follow                 peek at any new child processes too.\n"
"    -t, --tgid                   peek at all threads where tgid equals "
"<pid>.\n"
"    -d, --duplicates-removed     remove duplicate read/writes from the "
"output.\n"
"    -V, --version                prints version info.\n"
"    -h, --help                   prints this help.\n"
"\n"
"  Press CTRL-C to end output.\n"
msgstr ""
"Használat: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 8 bites tiszta adatfolyamok kiírása.\n"
"    -n ne jelenítse meg az írás/olvasást az fd fejlécekből.\n"
"    -c új gyermekfolyamatok megjelenítése.\n"
"    -d többszörös olvasások/írások eltávolítása a kimenetből.\n"
"    -V verzióinformációk kiírása.\n"
"    -h ezen súgó kiírása.\n"
"\n"
"  A CTRL-C segítségével megszakítható a kimenet.\n"

#: src/prtstat.c:54
#, c-format
msgid ""
"Usage: prtstat [options] PID ...\n"
"       prtstat -V\n"
"Print information about a process\n"
"    -r,--raw       Raw display of information\n"
"    -V,--version   Display version information and exit\n"
msgstr ""
"Használat: prtstat [kapcsolók] PID ...\n"
"           prtstat -V\n"
"Információk kiírása egy folyamatról\n"
"    -r,--raw       Az információk nyers megjelenítése\n"
"    -V,--version   Verzióinformációk kiírása és kilépés\n"

#: src/prtstat.c:65
#, c-format
msgid "prtstat (PSmisc) %s\n"
msgstr "prtstat (PSmisc) %s\n"

#: src/prtstat.c:66
#, fuzzy, c-format
msgid ""
"Copyright (C) 2009-2020 Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 2009 Craig Small\n"
"\n"

#: src/prtstat.c:78
msgid "running"
msgstr "fut"

#: src/prtstat.c:80
msgid "sleeping"
msgstr "alszik"

#: src/prtstat.c:82
msgid "disk sleep"
msgstr "lemez alszik"

#: src/prtstat.c:84
msgid "zombie"
msgstr "zombi"

#: src/prtstat.c:86
msgid "traced"
msgstr "követett"

#: src/prtstat.c:88
msgid "paging"
msgstr "lapoz"

#: src/prtstat.c:90
msgid "unknown"
msgstr "ismeretlen"

#: src/prtstat.c:164
#, c-format
msgid ""
"Process: %-14s\t\tState: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tThreads: %ld\n"
msgstr ""
"Folyamat: %-14s\t\tÁllapot: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tSzálak: %ld\n"

#: src/prtstat.c:169
#, c-format
msgid ""
"Process, Group and Session IDs\n"
"  Process ID: %d\t\t  Parent ID: %d\n"
"    Group ID: %d\t\t Session ID: %d\n"
"  T Group ID: %d\n"
"\n"
msgstr ""
"Folyamat, csoport és munkamenet-azonosítók\n"
"  Folyamat az.: %d\t\t   Szülő az.: %d\n"
"   Csoport az.: %d\t\t M.menet az.: %d\n"
" T Csoport az.: %d\n"
"\n"

#: src/prtstat.c:175
#, c-format
msgid ""
"Page Faults\n"
"  This Process    (minor major): %8lu  %8lu\n"
"  Child Processes (minor major): %8lu  %8lu\n"
msgstr ""
"Laphibák\n"
"  Ez a folyamat     (minor major): %8lu  %8lu\n"
"  Gyermekfolyamatok (minor major): %8lu  %8lu\n"

#: src/prtstat.c:180
#, c-format
msgid ""
"CPU Times\n"
"  This Process    (user system guest blkio): %6.2f %6.2f %6.2f %6.2f\n"
"  Child processes (user system guest):       %6.2f %6.2f %6.2f\n"
msgstr ""
"CPU idők\n"
"  Ez a folyamat     (felh. rendszer vendég blkio):  %6.2f %6.2f %6.2f %6.2f\n"
"  Gyermekfolyamatok (felh. rendszer vendég ):       %6.2f %6.2f %6.2f\n"

#: src/prtstat.c:189
#, c-format
msgid ""
"Memory\n"
"  Vsize:       %-10s\n"
"  RSS:         %-10s \t\t RSS Limit: %s\n"
"  Code Start:  %#-10lx\t\t Code Stop:  %#-10lx\n"
"  Stack Start: %#-10lx\n"
"  Stack Pointer (ESP): %#10lx\t Inst Pointer (EIP): %#10lx\n"
msgstr ""
"Memória\n"
"  Vméret:        %-10s\n"
"  RSS:           %-10s \t\t RSS korlát: %s\n"
"  Kód kezdete:   %#-10lx\t\t Kód vége:  %#-10lx\n"
"  Verem kezdete: %#-10lx\n"
"  Veremmutató (ESP): %#10lx\t Utasításmutató (EIP): %#10lx\n"

#: src/prtstat.c:199
#, c-format
msgid ""
"Scheduling\n"
"  Policy: %s\n"
"  Nice:   %ld \t\t RT Priority: %ld %s\n"
msgstr ""
"Ütemezés\n"
"  Irányelv: %s\n"
"  Nice:     %ld \t\t RT prioritás: %ld %s\n"

#: src/prtstat.c:220
msgid "asprintf in print_stat failed.\n"
msgstr "az asprintf a print_stat hívásban sikertelen.\n"

#: src/prtstat.c:225
#, c-format
msgid "Process with pid %d does not exist.\n"
msgstr "Nem létezik %d PID-ű folyamat.\n"

#: src/prtstat.c:227
#, c-format
msgid "Unable to open stat file for pid %d (%s)\n"
msgstr "Nem érhető el a fájl a(z) %d (%s) PID-hez\n"

#: src/prtstat.c:243
#, c-format
msgid "Unable to allocate memory for proc_info\n"
msgstr "Nem foglalható memória a proc_info részére\n"

#: src/prtstat.c:283
#, c-format
msgid "Unable to scan stat file"
msgstr "Nem olvasható be a stat fájl"

#: src/prtstat.c:318
msgid "Invalid option"
msgstr "Érvénytelen kapcsoló"

#: src/prtstat.c:323
msgid "You must provide at least one PID."
msgstr "Legalább egy PID-et meg kell adnia."

#: src/prtstat.c:327
#, c-format
msgid "/proc is not mounted, cannot stat /proc/self/stat.\n"
msgstr "a /proc nincs csatolva, nem érhető el a /proc/self/stat.\n"

#: src/pstree.c:1183
#, c-format
msgid "%s is empty (not mounted ?)\n"
msgstr "a(z) %s üres (nincs csatlakoztatva?)\n"

#: src/pstree.c:1216
#, c-format
msgid ""
"Usage: pstree [-acglpsStTuZ] [ -h | -H PID ] [ -n | -N type ]\n"
"              [ -A | -G | -U ] [ PID | USER ]\n"
"   or: pstree -V\n"
msgstr ""

#: src/pstree.c:1220
#, c-format
msgid ""
"\n"
"Display a tree of processes.\n"
"\n"
msgstr ""

#: src/pstree.c:1223
#, c-format
msgid ""
"  -a, --arguments     show command line arguments\n"
"  -A, --ascii         use ASCII line drawing characters\n"
"  -c, --compact-not   don't compact identical subtrees\n"
msgstr ""

#: src/pstree.c:1227
#, c-format
msgid ""
"  -C, --color=TYPE    color process by attribute\n"
"                      (age)\n"
msgstr ""

#: src/pstree.c:1230
#, c-format
msgid ""
"  -g, --show-pgids    show process group ids; implies -c\n"
"  -G, --vt100         use VT100 line drawing characters\n"
msgstr ""

#: src/pstree.c:1233
#, c-format
msgid ""
"  -h, --highlight-all highlight current process and its ancestors\n"
"  -H PID, --highlight-pid=PID\n"
"                      highlight this process and its ancestors\n"
"  -l, --long          don't truncate long lines\n"
msgstr ""

#: src/pstree.c:1238
#, c-format
msgid ""
"  -n, --numeric-sort  sort output by PID\n"
"  -N TYPE, --ns-sort=TYPE\n"
"                      sort output by this namespace type\n"
"                              (cgroup, ipc, mnt, net, pid, time, user, uts)\n"
"  -p, --show-pids     show PIDs; implies -c\n"
msgstr ""

#: src/pstree.c:1244
#, c-format
msgid ""
"  -s, --show-parents  show parents of the selected process\n"
"  -S, --ns-changes    show namespace transitions\n"
"  -t, --thread-names  show full thread names\n"
"  -T, --hide-threads  hide threads, show only processes\n"
msgstr ""

#: src/pstree.c:1249
#, c-format
msgid ""
"  -u, --uid-changes   show uid transitions\n"
"  -U, --unicode       use UTF-8 (Unicode) line drawing characters\n"
"  -V, --version       display version information\n"
msgstr ""

#: src/pstree.c:1253
#, fuzzy, c-format
msgid ""
"  -Z, --security-context\n"
"                      show security attributes\n"
msgstr ""
"  -Z, --security-context\n"
"                      SELinux biztonsági kontextusok megjelenítése\n"

#: src/pstree.c:1255
#, fuzzy, c-format
msgid ""
"\n"
"  PID    start at this PID; default is 1 (init)\n"
"  USER   show only trees rooted at processes of this user\n"
"\n"
msgstr ""
"    PID               kezdés ezzel a PID-del; az alapértelmezés az 1 (init)\n"
"    FELHASZNÁLÓ       csak az adott felhasználó folyamataiból kiinduló fák\n"
"                        megjelenítése\n"
"\n"

#: src/pstree.c:1263
#, c-format
msgid "pstree (PSmisc) %s\n"
msgstr "pstree (PSmisc) %s\n"

#: src/pstree.c:1383
#, c-format
msgid "TERM is not set\n"
msgstr "A TERM nincs beállítva\n"

#: src/pstree.c:1387
#, c-format
msgid "Can't get terminal capabilities\n"
msgstr "A terminál képességei nem kérhetők le\n"

#: src/pstree.c:1405
#, c-format
msgid "procfs file for %s namespace not available\n"
msgstr "procfs fájl nem érhető el a(z) %s névtérhez\n"

#: src/pstree.c:1452
#, c-format
msgid "No such user name: %s\n"
msgstr "Nincs ilyen felhasználónév: %s\n"

#: src/pstree.c:1478
#, c-format
msgid "No processes found.\n"
msgstr "Nem találhatók folyamatok.\n"

#: src/pstree.c:1486
#, c-format
msgid "Press return to close\n"
msgstr "A bezáráshoz nyomj entert\n"

#: src/signals.c:84
#, c-format
msgid "%s: unknown signal; %s -l lists signals.\n"
msgstr "%s: ismeretlen szignál; a %s -l felsorolja a szignálokat.\n"

#, c-format
#~ msgid ""
#~ "Usage: killall [ -Z CONTEXT ] [ -u USER ] [ -y TIME ] [ -o TIME ] [ -"
#~ "eIgiqrvw ]\n"
#~ "               [ -s SIGNAL | -SIGNAL ] NAME...\n"
#~ msgstr ""
#~ "Használat: killall [-Z KONTEXTUS] [-u FELHASZNÁLÓ] [ -y IDŐ ] [ -o IDŐ ] "
#~ "[ -eIgiqrvw ]\n"
#~ "               [ -s SZIGNÁL | -SZIGNÁL ] NÉV...\n"

#, c-format
#~ msgid ""
#~ "Copyright (C) 1993-2014 Werner Almesberger and Craig Small\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 1993-2014 Werner Almesberger és Craig Small\n"
#~ "\n"

#, c-format
#~ msgid ""
#~ "Usage: pstree [-acglpsStuZ] [ -h | -H PID ] [ -n | -N type ]\n"
#~ "Usage: pstree [-acglpsStu] [ -h | -H PID ] [ -n | -N type ]\n"
#~ "              [ -A | -G | -U ] [ PID | USER ]\n"
#~ "       pstree -V\n"
#~ "Display a tree of processes.\n"
#~ "\n"
#~ "  -a, --arguments     show command line arguments\n"
#~ "  -A, --ascii         use ASCII line drawing characters\n"
#~ "  -c, --compact       don't compact identical subtrees\n"
#~ "  -h, --highlight-all highlight current process and its ancestors\n"
#~ "  -H PID,\n"
#~ "  --highlight-pid=PID highlight this process and its ancestors\n"
#~ "  -g, --show-pgids    show process group ids; implies -c\n"
#~ "  -G, --vt100         use VT100 line drawing characters\n"
#~ "  -l, --long          don't truncate long lines\n"
#~ "  -n, --numeric-sort  sort output by PID\n"
#~ "  -N type,\n"
#~ "  --ns-sort=type      sort by namespace type (ipc, mnt, net, pid, user, "
#~ "uts)\n"
#~ "  -p, --show-pids     show PIDs; implies -c\n"
#~ "  -s, --show-parents  show parents of the selected process\n"
#~ "  -S, --ns-changes    show namespace transitions\n"
#~ "  -t, --thread-names  show full thread names\n"
#~ "  -T, --hide-threads  hide threads, show only processes\n"
#~ "  -u, --uid-changes   show uid transitions\n"
#~ "  -U, --unicode       use UTF-8 (Unicode) line drawing characters\n"
#~ "  -V, --version       display version information\n"
#~ msgstr ""
#~ "Használat: pstree [-acglpsStuZ] [ -h | -H PID ] [ -n | -N típus ]\n"
#~ "Használat: pstree [-acglpsStu] [ -h | -H PID ] [ -n | -N típus ]\n"
#~ "                  [ -A | -G | -U ] [ PID | FELHASZNÁLÓ ]\n"
#~ "           pstree -V\n"
#~ "Megjeleníti a folyamatok fáját.\n"
#~ "\n"
#~ "  -a, --arguments     parancssori argumentumok megjelenítése\n"
#~ "  -A, --ascii         ASCII sorrajzoló karakterek használata\n"
#~ "  -c, --compact       azonos részfákat ne tömörítse\n"
#~ "  -h, --highlight-all az aktuális folyamat és őseinek kiemelése\n"
#~ "  -H PID, \n"
#~ "  --highlight-pid=PID az adott folyamat és őseinek kiemelése\n"
#~ "  -g, --show-pgids    folyamatcsoport-azonosítók megjelenítése,\n"
#~ "                        magába foglalja a -c kapcsolót\n"
#~ "  -G, --vt100         VT100 sorrajzoló karakterek használata\n"
#~ "  -l, --long          ne csonkítsa a hosszú sorokat\n"
#~ "  -n, --numeric-sort  kimenet rendezése PID szerint\n"
#~ "  -N típus,\n"
#~ "  --ns-sort=típus     névtértípus szerinti rendezés (ipc, mnt, net, pid,\n"
#~ "                        user, uts)\n"
#~ "  -p, --show-pids     a PID-ek megjelenítése; magába foglalja a -c "
#~ "kapcsolót\n"
#~ "  -s, --show-parents  a kiválasztott folyamat szülőinek megjelenítése\n"
#~ "  -S, --ns-changes    névtér átmenetek megjelenítése\n"
#~ "  -t, --thread-names  teljes szálnevek megjelenítése\n"
#~ "  -T, --hide-threads  szálak elrejtése, csak folyamatok megjelenítése\n"
#~ "  -u, --uid-changes   az uid átmenetek megjelenítése\n"
#~ "  -U, --unicode       UTF-8 (Unicode) sorrajzoló karakterek használata\n"
#~ "  -V, --version       verzióinformációk megjelenítése\n"

#, c-format
#~ msgid ""
#~ "Copyright (C) 1993-2009 Werner Almesberger and Craig Small\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 1993-2009 Werner Almesberger és Craig Small\n"
#~ "\n"
